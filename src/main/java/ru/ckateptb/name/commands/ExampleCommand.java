package ru.ckateptb.name.commands;

import com.google.common.collect.Lists;
import org.bukkit.command.CommandSender;
import ru.ckateptb.api.command.TableCommand;
import ru.ckateptb.name.Name;
import ru.ckateptb.name.configs.Config;
import ru.ckateptb.name.configs.Language;

import java.util.Arrays;
import java.util.List;

public class ExampleCommand extends TableCommand {

    //TODO ЭТОТ КЛАСС ПРИМЕР СОЗДАНИЯ КОМАНДЫ

    public ExampleCommand(String... cmd) {
        super(cmd);
    }

    @Override
    public boolean progress(CommandSender sender, String[] args) {
        if (sender.hasPermission(Name.NAME + "." + this.getName()) && args.length == 1 && args[0].equalsIgnoreCase("reload")) {
            Name.setupConfigs(); //TODO ТАК ПЕРЕЗАПУСКАЕТСЯ КОНФИГ
            return true;
        } else {
            sender.sendMessage(Language.EXAMPLE);
            sender.sendMessage(Language.LIST.get(1) + Config.NUMBER);
            return true;
        }
    }

    @Override
    public List<String> tab(CommandSender sender, String[] args) {
        return "reload".startsWith(args[0]) && args.length == 1 ? Arrays.<String>asList("reload") : Lists.newLinkedList();
    }
}
