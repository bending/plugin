package ru.ckateptb.name.configs;

import ru.ckateptb.api.config.Configurable;

import java.util.Arrays;
import java.util.List;

public class Language extends Configurable {

    //TODO ЭТОТ КЛАСС ПРИМЕР СОЗДАНИЯ КОНФИГА

    @Node("message.example")
    public static String EXAMPLE = "ПРИМЕР";

    @Node("message.answer")
    public static List<String> LIST = Arrays.<String>asList("ПРИМЕР", "ЛИСТА№");

}
