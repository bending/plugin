package ru.ckateptb.name;

import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.java.annotation.plugin.Description;
import ru.ckateptb.api.config.ConfigManager;
import ru.ckateptb.name.commands.ExampleCommand;
import ru.ckateptb.name.configs.Config;
import ru.ckateptb.name.configs.Language;
import ru.ckateptb.name.listeners.Handler;

@org.bukkit.plugin.java.annotation.plugin.Plugin(name = Name.NAME, version = Name.VERSION)
@Description("Основа вашего плагина")
public class Name extends JavaPlugin {

    public static final String NAME = "Name";
    public static final String VERSION = "-dev";
    public static final ConfigManager.Manager config = new ConfigManager.Manager();
    ;
    @Getter
    private static Name plugin;

    @Override
    public void onEnable() {
        plugin = this;
        setupConfigs();
        new ExampleCommand("ExampleCommand", "ExampleAliases"); //TODO РЕГИСТРАЦИЯ КОМАНДЫ
        Bukkit.getPluginManager().registerEvents(new Handler(), plugin); //TODO РЕГИСТРАЦИЯ ИВЕНТХЕНДЛЕРА
    }

    //TODO РЕГИСТРАЦИЯ КОНФИГА
    public static void setupConfigs() {
        config.setupConfig(plugin, "config.yml", Config.class);
        config.setupConfig(plugin, "language.yml", Language.class);
    }
}
